#include <iostream>
#include <conio.h>
#include <vector>
using namespace std;

void firstFit(int partitions [], vector <int> jobs);
void printPartitions(int partitions [], int result [], vector <int> waitList);

void main()
{

	int partitions [5];
	int input; 
	vector <int> jobs;
	for (int i = 0; i < 5; i++)
	{
		cout << "Please enter size for partition " << i+1 << endl;
		cin >> partitions[i];
	}


	do
	{
		cout << "Please enter the size of a job (9999 to stop): ";
		cin >> input;
		if (input != 9999)
		{
			jobs.push_back(input);
		}
	} while (input != 9999);
	
	jobs.shrink_to_fit(); //shrinks vector array down to size of jobs.
	firstFit(partitions, jobs);
	
	cout << endl << "Press any key to continue..." << endl;
	_getch(); //waits for user input to exit.
}

void firstFit(int partitions [], vector <int> jobs)
{
	int initialPartition[5];
	copy(partitions, partitions+5, initialPartition);

	int result[5] = {0};
	vector <int> waitList;
	for (int j = 0; j < jobs.size(); j++)
	{
		for (int p = 0; p < 5; p++)
		{
			if (partitions[p] != -1 && jobs[j] != -1 && jobs[j] <= partitions[p]) 
				//check if partition is filled or if job is accepted already
					//then check if job can fit in first available partition.
			{
				result[p]= jobs[j];
				jobs[j] = -1; //job has been matched, remove from listing. 
				partitions[p] = -1; //partition is now filled.
			}
		}
		if (jobs[j] != -1) //if the job still remains after checking all partitions, put on waitlist. 
			waitList.push_back(jobs[j]);
	}

	printPartitions(initialPartition, result, waitList);
}

void printPartitions(int partitions [], int result [], vector <int> waitList)
{
	for (int i = 0; i < 5; i++)
	{
		if (result[i] != 0)
			cout << "Job " << i+1 << " of size: " << result[i] << " in partition: " << i+1 << " of size: " << partitions[i] << endl;
	}

	for (int i = 0; i < waitList.size(); i++)
	{
		cout << "Job " << i+1 << " of size: " << waitList[i] << " waits" << endl; 
	}
}

