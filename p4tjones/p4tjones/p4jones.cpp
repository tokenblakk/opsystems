/*
* Programming assignment 4 
* Tramel Jones
* OpSystems 4335
* GSU Fa13
*/

#include <iostream>
#include <conio.h>
#include <vector>
using namespace std;

//Function prototypes
void firstFit(vector <int> pageList);
void printPartitions(int partitions [], int result [], vector <int> waitList);
vector <int> parseRefs(vector <int> refs);
void insertInEmptyFrame(int currentPage);
void printPages();
void printFrames();

//Class Definitions
class Page
{
	bool valid;
	int frame;

public:
	Page() {frame = -1; valid = false;}
	Page(int f) {set_frame(f);}
	Page(int f, bool v) {set_frame(f); set_valid(v);}
	void set_frame(int f){frame = f; valid = true;}
	void set_valid(bool v) {valid = v;}
	bool isValid(){return valid;}
	int getFrame(){return frame;}
};

//Global Variables
vector <Page> pages;
vector <int> frame;
int frameCtr;
#define PAGESIZE 100;

void main()
{
	int input; 
	vector <int> refs;
	
	//set initial size of vectors
	pages.resize(10);
	frame.resize(3);
	frameCtr = 0;

	for(int i = 0; i<10; i++)
	{
		pages[i] = Page();
	}

	for(int i = 0; i<3; i++)
	{
		frame[i] = -1;
	}

	//Prompt User
	do
	{
		cout << "Please enter a reference (9999 to exit): ";
		cin >> input;
		if (input != 9999)
		{
			refs.push_back(input);
		}
	} while (input != 9999);
	
	refs.shrink_to_fit(); //shrinks vector array down to size of jobs.
	
	vector <int> pagelist = parseRefs(refs);
	firstFit(pagelist);
	cout << endl;
	printPages();
	cout << endl;
	printFrames();
	
	cout << endl << "Press any key to continue..." << endl;
	_getch(); //waits for user input to exit.
}


vector <int> parseRefs(vector <int> refs)
	//Determine Page numbers for each reference in list
{
	vector <int> pagelist;
	for (int i = 0; i < refs.size(); i++)
	{
		int refPage = refs[i]/PAGESIZE; //Integer division gives proper page number
		cout <<"Ref "<< i << " is in page: " << refPage << endl;
		pagelist.push_back(refPage);
	}
	cout << endl;
	return pagelist;
}

void firstFit(vector <int> pagelist)
{
	//Place page in first available frame
	for (int i =0; i < pagelist.size(); i++) 
	{
		int currentPage = pagelist[i];
			if (!pages[currentPage].isValid()) //new page validation
			{
				insertInEmptyFrame(currentPage);					
			}
			else //page already validated
			{
				if (frame[i] == currentPage) //Check frame to see if page is still there
					;//Do Nothing
			}
	}
}

void insertInEmptyFrame(int currentPage)
{
//Check to see if any frames are available
	for (int j = 0; j < frame.size(); j++)
	{
		if (frame[j] == -1) //frame is empty
		{
			//validate new page
			pages[currentPage].set_frame(j);
			frame[j] = currentPage;
			//increase frame counter
			frameCtr = (frameCtr  + 1) %3;
			return;
		}
	}
	for (int j = 0; j < frame.size(); j++)
	{
		if (frame[j] != -1) //FIFO Replacement
		{
			//invalidate page
			pages[frame[frameCtr]].set_valid(false);
			pages[frame[frameCtr]].set_frame(-1);
			//validate new page
			pages[currentPage].set_frame(frameCtr);
			//give new page a home
			frame[frameCtr] = currentPage;
			//increase frame counter
			frameCtr = (frameCtr  + 1) %3;
			return;
		}
	}		
}

void printPages()
{
	for (int i = 0; i < 10; i++)
	{
		if (pages[i].getFrame() != -1)
			cout << "Page " << i << " is in frame " << pages[i].getFrame() << endl;
	}
}

void printFrames()
{
	for (int i = 0; i < 3; i++)
	{
			cout << "Frame " << i << " contains page " << frame[i] << endl;
	}
}
