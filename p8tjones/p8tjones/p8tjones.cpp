/*
* Programming assignment 8
* Tramel Jones
* OpSystems 4335
* GSU Fa13
*/


//Consumer Producer Process Management using threading and mutex
#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <windows.h>
#include <vector>
#include <thread>
using namespace std;
//thread a;
//thread b;
vector <int> inputBuffer;
volatile HANDLE mySem = NULL;
thread a;
thread b;
boolean bStop = FALSE;
int counter;
#define THREADCOUNT 2;


//Prototype
void producer();
void consumer();
void AttemptToStop();
void printBanner();

//Assignment Banner
void printBanner()
{
	cout << "Tramel Jones" << endl;
	cout << "CPSC 4335 Fa2013" << endl;
	cout << "Programming Assignment 8" << endl;
	cout << "Concurrent Threading using Semaphores. Producer/Consumer problem" << endl;
	cout << "------------------------------------------------"<< endl << endl;
}


//Threads
void producer()
{
	int tempInt;
	for (;;)
	{
		if (bStop == TRUE) { ExitThread(0); }

		WaitForSingleObject(mySem, INFINITE);
		
		
		printf("Feed me a number! (-999 to exit) \n");
		cin >> tempInt;
		if (tempInt == -999)
			AttemptToStop();
		inputBuffer.push_back(tempInt);
		Sleep(5); // sleep for 5ms
		ReleaseSemaphore(mySem, 1, NULL);
	}

}
void consumer()
{
	Sleep(500);

	for (;;)
	{

		if (bStop == TRUE) { ExitThread(0); }

		WaitForSingleObject(mySem, INFINITE);
		cout <<"You fed me: "<< inputBuffer[counter] << endl;
		counter++;
		printf("Nom! \n\n");
		Sleep(5); // sleep for 5ms
		ReleaseSemaphore(mySem, 1, NULL);
	}
}

//Stop the threads!
void AttemptToStop()
{
	
	printf("Attempting to stop all threads...\n\a");
	{
		bStop = TRUE;
	}
	cout << endl << "Press any key to continue..." << endl;
	_getch(); //waits for user input to exit.
	ExitProcess(0);

	
	// wait for all threads to finish -- screw that, end the entire process...
	//****************WaitForMultipleObjects(THREADCOUNT, (CONST HANDLE *)mySem, TRUE, INFINITE);
}


void main()
{
	printBanner();
	
	counter = 0;
	mySem = CreateSemaphore(NULL, 1, 1, NULL);
	thread a = thread(consumer);
	thread b = thread(producer);

	a.join();
	b.join();
	Sleep(5000000);
	CloseHandle(mySem);
	

	cout << endl << "Press any key to continue..." << endl;
	_getch(); //waits for user input to exit.
	ExitProcess(0);
}

